//
//  Car.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSObject

// Properties
@property (strong, nonatomic) NSString* model;
@property (strong, nonatomic) UIColor*  color;

@end

NS_ASSUME_NONNULL_END
