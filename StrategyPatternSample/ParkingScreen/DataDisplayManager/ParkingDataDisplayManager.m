//
//  ParkingDataDisplayManager.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ParkingDataDisplayManager.h"

// Classes
#import "Car.h"
#import "ParkingCarPlaceCell.h"

// Constants
#import "ParkingScreenConstants.h"

@interface ParkingDataDisplayManager()

// Properties
@property (strong, nonatomic) NSArray* carsArray;

@end

@implementation ParkingDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithDataSource: (id<DataDisplayManagerParkingInput>)  dataSource
                         withOutput: (id<DataDisplayManagerParkingOutput>) output
{
    if ( self == [super init] )
    {
        self.dataSource = dataSource;
        self.output     = output;
    }
    
    return self;
}


#pragma mark - Properties -

- (NSArray*) carsArray
{
    if ( _carsArray == nil )
    {
        _carsArray = [self.dataSource fetchCollectionContent];
    }
    
    return _carsArray;
}


#pragma mark - UICollectionViewDataSource methods -

- (NSInteger) numberOfSectionsInCollectionView: (UICollectionView*) collectionView
{
    return 1;
}

- (NSInteger) collectionView: (UICollectionView*) collectionView
      numberOfItemsInSection: (NSInteger)         section
{
    return self.carsArray.count;
}

- (UICollectionViewCell*) collectionView: (UICollectionView*) collectionView
                  cellForItemAtIndexPath: (NSIndexPath*)      indexPath
{
    Car* car = self.carsArray[indexPath.item];
    
    ParkingCarPlaceCell* parkingCarPlaceCell = [collectionView dequeueReusableCellWithReuseIdentifier: kParkingCarPlaceCellIdentifier
                                                                             forIndexPath: indexPath];
    
    [parkingCarPlaceCell fillContentWithCar: car];
    
    return parkingCarPlaceCell;
}


#pragma mark - Public methods -

- (NSArray*) currentData
{
    return self.carsArray;
}

- (void) fillNewData: (NSArray*) data
{
    self.carsArray = data;
    
    [self.dataSource fillCollectionContent: data];
}

@end
