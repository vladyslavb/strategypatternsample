//
//  ParkingCarPlaceCell.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ParkingCarPlaceCell.h"

@interface ParkingCarPlaceCell()

// Outlets
@property (weak, nonatomic) IBOutlet UIImageView* carImageView;
@property (weak, nonatomic) IBOutlet UILabel*     carModelLabel;

@end

@implementation ParkingCarPlaceCell


#pragma mark - Life cycle -

- (void) awakeFromNib
{
    [super awakeFromNib];
}


#pragma mark - Public methods -

- (void) fillContentWithCar: (Car*) car
{
    self.carModelLabel.text = car.model;
    self.backgroundColor    = car.color;
}

@end
