//
//  DataDisplayManagerParkingOutput.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DataDisplayManagerParkingOutput <NSObject>

@end

NS_ASSUME_NONNULL_END
