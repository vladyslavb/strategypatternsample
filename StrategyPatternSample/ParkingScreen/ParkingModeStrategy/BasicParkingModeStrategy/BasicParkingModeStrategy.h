//
//  BasicParkingModeStrategy.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Classes
#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface BasicParkingModeStrategy : NSObject


#pragma mark - Abstract Overrided methods -

- (void) syncParkingWithCarsCollection: (UICollectionView*) collection;

@end

NS_ASSUME_NONNULL_END
