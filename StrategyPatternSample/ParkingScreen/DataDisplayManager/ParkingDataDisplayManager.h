//
//  ParkingDataDisplayManager.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Protocols
#import "DataDisplayManagerParkingInput.h"
#import "DataDisplayManagerParkingOutput.h"

NS_ASSUME_NONNULL_BEGIN

@interface ParkingDataDisplayManager : NSObject <UICollectionViewDataSource, UICollectionViewDelegate>

// Properties
@property (weak, nonatomic) id<DataDisplayManagerParkingInput>  dataSource;
@property (weak, nonatomic) id<DataDisplayManagerParkingOutput> output;

// Methods

/**
 @author Vladyslav Bedro
 
 Method initializes and binds components of the MVC pattern
 */
- (instancetype) initWithDataSource: (id<DataDisplayManagerParkingInput>)  dataSource
                         withOutput: (id<DataDisplayManagerParkingOutput>) output;

/**
 @author Vladyslav Bedro
 
 Method returns cars array from data display manager of collection controller
 */
- (NSArray*) currentData;


/**
 @author Vladyslav Bedro
 
 Method set cars array for data display manager of collection controller
 */
- (void) fillNewData: (NSArray*) data;

@end

NS_ASSUME_NONNULL_END
