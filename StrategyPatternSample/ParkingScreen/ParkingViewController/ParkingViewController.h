//
//  ParkingViewController.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

// Protocols
#import "DataDisplayManagerParkingOutput.h"

NS_ASSUME_NONNULL_BEGIN

@interface ParkingViewController : UIViewController <DataDisplayManagerParkingOutput>

// Properties
@property (weak, nonatomic) id<DataDisplayManagerParkingOutput> output;

@end

NS_ASSUME_NONNULL_END
