//
//  BasicParkingModeStrategy.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BasicParkingModeStrategy.h"

@implementation BasicParkingModeStrategy


#pragma mark - Abstract Overrided methods -

- (void) syncParkingWithCarsCollection: (UICollectionView*) collection {}

@end
