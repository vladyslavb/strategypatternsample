//
//  ParkingScreenConstants.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#ifndef ParkingScreenConstants_h
#define ParkingScreenConstants_h

static NSString* kParkingCarPlaceCellNibName    = @"ParkingCarPlaceCell";
static NSString* kParkingCarPlaceCellIdentifier = @"ParkingCarPlaceCellID";

#endif /* ParkingScreenConstants_h */
