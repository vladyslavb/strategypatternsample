//
//  NightParkingModeStrategy.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "NightParkingModeStrategy.h"

@implementation NightParkingModeStrategy


#pragma mark - Abstract Overrided methods -
/*
    Ожидается много специфичного кода для данного режима парковки
 */
- (void) syncParkingWithCarsCollection: (UICollectionView*) collection
{
    collection.backgroundColor = [UIColor whiteColor];
}

@end
