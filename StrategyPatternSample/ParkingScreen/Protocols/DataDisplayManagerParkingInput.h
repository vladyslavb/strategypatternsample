//
//  DataDisplayManagerParkingInput.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DataDisplayManagerParkingInput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method fills collection with Car objects
 
 @param carsArray - array with new car objects
 */
- (void) fillCollectionContent: (NSArray*) carsArray;

/**
 @author Vladyslav Bedro
 
 Method returns all current cars
 
 @return array with all cars objects
 */
- (NSArray*) fetchCollectionContent;

@end

NS_ASSUME_NONNULL_END
