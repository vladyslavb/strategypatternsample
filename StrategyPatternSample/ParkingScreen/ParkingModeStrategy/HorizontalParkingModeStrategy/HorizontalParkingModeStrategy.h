//
//  HorizontalParkingModeStrategy.h
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "BasicParkingModeStrategy.h"

// Classes
#import "BasicParkingModeStrategy.h"

NS_ASSUME_NONNULL_BEGIN

@interface HorizontalParkingModeStrategy : BasicParkingModeStrategy

@end

NS_ASSUME_NONNULL_END
