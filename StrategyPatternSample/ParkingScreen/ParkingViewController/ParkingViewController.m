//
//  ParkingViewController.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ParkingViewController.h"

// Classes
#import "ParkingModel.h"
#import "ParkingDataDisplayManager.h"

#import "BasicParkingModeStrategy.h"
#import "VerticalParkingModeStrategy.h"
#import "NightParkingModeStrategy.h"
#import "HorizontalParkingModeStrategy.h"

// Constant files
#import "ParkingScreenConstants.h"

@interface ParkingViewController ()

// Outlets
@property (weak, nonatomic) IBOutlet UICollectionView* parkingCollection;

// Properties
@property (strong, nonatomic) ParkingModel*              model;
@property (strong, nonatomic) ParkingDataDisplayManager* displayManager;

@property (strong, nonatomic) BasicParkingModeStrategy* currentParkingModeStrategy;

@end

@implementation ParkingViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureParkingCollection];
}


#pragma mark - Properties -

- (ParkingDataDisplayManager*) displayManager
{
    if ( _displayManager == nil )
    {
        _displayManager = [[ParkingDataDisplayManager alloc] initWithDataSource: self.model
                                                                     withOutput: self];
    }
    
    return _displayManager;
}

- (ParkingModel*) model
{
    if ( _model == nil )
    {
        _model = [ParkingModel new];
    }
    
    return _model;
}


#pragma mark - binding UI -

- (void) configureParkingCollection
{
    self.parkingCollection.dataSource = self.displayManager;
    self.parkingCollection.delegate   = self.displayManager;
    
    [self.parkingCollection registerNib: [UINib nibWithNibName: kParkingCarPlaceCellNibName
                                                        bundle: nil]
             forCellWithReuseIdentifier: kParkingCarPlaceCellIdentifier];
}


#pragma mark - Actions -

- (IBAction) onVerticalDirectionParkingModeButtonPressed: (UIButton*) sender
{
    self.currentParkingModeStrategy = [[VerticalParkingModeStrategy alloc] init];
}

- (IBAction) onNightParkingModeButtonPressed: (UIButton*) sender
{
    self.currentParkingModeStrategy = [[NightParkingModeStrategy alloc] init];
}

- (IBAction) onHorizontalDirectionParkingMode: (UIButton*) sender
{
    self.currentParkingModeStrategy = [[HorizontalParkingModeStrategy alloc] init];
}

- (IBAction) onUpdateButtonPressed: (UIButton*) sender
{
    /*
        Благодаря паттерна 'Strategy' тут не нужно делать множество проверок на преднадлежность к классу текущего режима (одна из основных задач паттерна - замена множественных if/switch-конструкций)
     */
    if ( self.currentParkingModeStrategy )
    {
        [self.currentParkingModeStrategy syncParkingWithCarsCollection: self.parkingCollection];
    }
}


@end
