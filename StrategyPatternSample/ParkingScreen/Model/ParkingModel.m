//
//  ParkingModel.m
//  StrategyPatternSample
//
//  Created by Vladyslav Bedro on 12/26/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "ParkingModel.h"

// Classes
#import "Car.h"

@interface ParkingModel()

// Properties
@property (strong, nonatomic) NSMutableArray* carsArray;

@end

@implementation ParkingModel


#pragma mark - Properties -

- (NSMutableArray*) carsArray
{
    if ( _carsArray == nil )
    {
        _carsArray = [NSMutableArray new];
        
        Car* sportCar     = [[Car alloc] init];
        Car* busCar       = [[Car alloc] init];
        Car* jeepCar      = [[Car alloc] init];
        Car* hatchbackCar = [[Car alloc] init];
        
        sportCar.model     = @"Porshe911";
        busCar.model       = @"VolvoBUS";
        jeepCar.model      = @"LandRoverJeep";
        hatchbackCar.model = @"Opel123";
        
        sportCar.color     = [UIColor redColor];
        busCar.color       = [UIColor grayColor];
        jeepCar.color      = [UIColor blackColor];
        hatchbackCar.color = [UIColor greenColor];
        
        [_carsArray addObject: sportCar];
        [_carsArray addObject: busCar];
        [_carsArray addObject: jeepCar];
        [_carsArray addObject: hatchbackCar];
    }
    
    return _carsArray;
}


#pragma mark - Display manager protocol methods -

- (NSArray*) fetchCollectionContent
{
    return [NSArray arrayWithArray: self.carsArray];
}

- (void) fillCollectionContent: (NSArray*) carsArray;
{
    self.carsArray = carsArray.mutableCopy;
}

@end
